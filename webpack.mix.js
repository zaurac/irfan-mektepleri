let mix = require('laravel-mix');
let critical = require('laravel-mix-critical');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
const Public = 'public/';

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ])
    .combine(
        [
            Public + 'assets/plugins/bootstrap/js/bootstrap.min.js',
            Public + 'assets/plugins/jquery/jquery.min.js',
            Public + 'assets/js/lazyLoad.min.js',
            Public + 'assets/js/script.js',
        ],Public + 'js/main.min.js')
    .version();