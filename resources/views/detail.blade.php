@extends('layout.master')
@section('content')
    <article class="container-fluid bg-primary-color-two pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center mb-4">
                    @if(!empty($value->subtitle))<h2 class="subtitle">{{$value->subtitle}}</h2>@endif
                    <h1 class="mainTitle fw-bold">{{$value->title}}</h1>
                </div>
            </div>
        </div>
    </article>

    <article class="container-fluid pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-left mb-4">
                    <div class="col-12">
                        {!! $value->content !!}
                    </div>
                </div>
            </div>
        </div>
    </article>
@endsection