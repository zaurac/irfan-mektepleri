<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @php
        Artesaos\SEOTools\Facades\OpenGraph::setTitle(SEOMeta::getTitle());
        Artesaos\SEOTools\Facades\OpenGraph::setDescription(SEOMeta::getDescription());
        SEOMeta::setCanonical(request()->url());
    @endphp

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}

    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="preload" as="style" href="{{asset('assets/css/style.min.css')}}" onload="this.rel='stylesheet'">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.min.css')}}">

    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/images/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('assets/images/favicon/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('assets/images/favicon/safari-pinned-tab.svg')}}" color="#6e3a3a">
    <meta name="msapplication-TileColor" content="#6e3a3a">
    <meta name="theme-color" content="#6e3a3a">
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <nav class="navbar sticky-top navbar-expand-lg">
                    <div class="container-fluid">
                        <a class="navbar-brand logo" href="{{route('index')}}">
                            {!! WebpConvert::createTag('assets/images/logo.png',['resize' => '1','width' =>[203,250], 'height' => [35,43]],['alt' => 'İrfan Mektepleri','title' => 'İrfan Mektepleri'],'','logo-h') !!}
                        </a>
                        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item"><a class="nav-link {{ Route::currentRouteName() === 'index' ? 'active' : '' }}" aria-current="page" href="{{route('index')}}">Anasayfa</a></li>
                                <li class="nav-item"><a class="nav-link {{ Route::currentRouteName() === 'biography' ? 'active' : '' }}" href="{{route('biography')}}">Biyografi</a></li>
                                <li class="nav-item"><a class="nav-link {{ Route::currentRouteName() === 'contents' ? 'active' : '' }}" href="{{route('contents')}}">İçindekiler</a></li>
                                <li class="nav-item"><a class="nav-link {{ Route::currentRouteName() === 'contact' ? 'active' : '' }}" href="{{route('contact')}}">İletişim</a></li>
                            </ul>

                            <div class="social-media d-inline d-lg-none">
                                <ul>
                                    @if(!empty(setting('site.facebook')))<li><a href="{{setting('site.facebook')}}" target="_blank" class="facebook"></a></li>@endif
                                    @if(!empty(setting('site.twitter')))<li><a href="{{setting('site.twitter')}}" target="_blank" class="twitter"></a></li>@endif
                                    @if(!empty(setting('site.instagram')))<li><a href="{{setting('site.instagram')}}" target="_blank" class="instagram"></a></li>@endif
                                    @if(!empty(setting('site.youtube')))<li><a href="{{setting('site.youtube')}}" target="_blank" class="youtube"></a></li>@endif
                                </ul>
                            </div>
                        </div>

                    </div>
                </nav>
            </div>
        </div>
    </header>
    @yield('content')
    <footer class="container-fluid">
        <div class="container">
            <div class="row justify-content-center ">
                <div class="col-lg-5">
                    <div class="logo text-center text-lg-start">
                        <a href="{{route('index')}}">
                            {!! WebpConvert::createTag('assets/images/logo.png',['width' =>[203,406], 'height' => [35,70]],['alt' => 'İrfan Mektepleri','title' => 'İrfan Mektepleri'],'','logo-footer') !!}
                        </a>
                    </div>
                    <div class="d-none d-lg-inline">
                        <p>ISBN: 978-605-4577-63-7</p>
                        <hr>
                        <p>© Kurtuba Kitap 2022 Kitabın tamamı yada bir bölümü izinsiz olarak hiçbir biçimde çoğaltılamaz, dağıtılamaz.</p>
                    </div>
                </div>

                <div class="col-lg-7 row justify-content-center ">
                    <div class="col-xl-4 col-md-6 col-8 mb-5 mt-5 mt-lg-0 mb-xl-0">
                        <div class="menu social-media">
                            <p class="title">Sosyal Medya</p>
                            <ul>
                                @if(!empty(setting('site.facebook')))<li><a href="{{setting('site.facebook')}}" target="_blank" class="facebook">facebook</a></li>@endif
                                @if(!empty(setting('site.twitter')))<li><a href="{{setting('site.twitter')}}" target="_blank" class="twitter">twitter</a></li>@endif
                                @if(!empty(setting('site.instagram')))<li><a href="{{setting('site.instagram')}}" target="_blank" class="instagram">instagram</a></li>@endif
                                @if(!empty(setting('site.youtube')))<li><a href="{{setting('site.youtube')}}" target="_blank" class="youtube">youtube</a></li>@endif
                            </ul>
                        </div>
                    </div>

                    <div class="w-100 d-sm-none"></div>

                    <div class="col-xl-4 col-md-6 col-8 mb-5 mt-5 mt-lg-0 mb-xl-0">
                        <div class="menu">
                            <p class="title">İçerik</p>
                            <ul>
                                @foreach($footerContents as $footerContent)
                                    <li><a href="{{route('content.detail',$footerContent->slug)}}">{{$footerContent->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="w-100 d-sm-none"></div>

                    <div class="col-xl-4 col-md-6 col-8 mb-5 mb-lg-0">
                        <div class="menu">
                            <p class="title">Site Haritası</p>
                            <ul>
                                <li><a href="{{route('index')}}">Anasayfa</a></li>
                                <li><a href="{{route('biography')}}">Biyografi </a></li>
                                <li><a href="{{route('contents')}}">İçindekiler</a></li>
                                <li><a href="{{route('contact')}}">İletişim</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="w-100 d-sm-none"></div>

                    <div class="d-inline col-12 d-lg-none">
                        <p>ISBN: 978-605-4577-63-7</p>
                        <hr>
                        <p>© Kurtuba Kitap 2022 Kitabın tamamı yada bir bölümü izinsiz olarak hiçbir biçimde çoğaltılamaz, dağıtılamaz.</p>
                    </div>
                </div>

                <p class="copyright">© 2023 İrfan Mektepleri Yayın Kurulu Tarafından Tüm Hakları Saklıdır.</p>
            </div>
        </div>
    </footer>
</body>
<script src="{{mix('js/main.min.js')}}"></script>

</html>