@extends('layout.master')
@section('content')
    <article class="container-fluid bg-primary-color-two pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center mb-4">
                    <h1 class="subtitle">İrfan Mektepleri İçindekiler</h1>
                    <p class="mainTitle fw-bold">Konu Başlıkları</p>
                </div>
            </div>
        </div>
    </article>

    <article class="container-fluid contents">
        <div class="container">
            <div class="row">
                @foreach($values as $key => $value)
                    <div class="col-xl-6">
                        <div class="item row">
                            <div class="col-2 d-none d-lg-inline number">{{ $key + 1 }}</div>
                            <div class="col-12 col-lg-10 content">
                                <h2 class="title">{{$value->title}}</h2>
                                <div>
                                    {!! $value->summary !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </article>
@endsection