@extends('layout.master')
@section('content')
    <article class="container-fluid bg-primary-color-two biography">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center mb-4">
                    {!! WebpConvert::createTag('upload/'.$value->image,['width' =>[262], 'height' => [288]],['class' => 'avatar','alt' => $value->title ?? 'İrfan Mektepleri','title' => $value->title ?? 'İrfan Mektepleri'],'',$value->title.'-icon') !!}
                    @if(!empty($value->subtitle))<h2 class="sub-title">{{$value->subtitle}}</h2>@endif
                    <h1 class="main-title">{{$value->title}}</h1>
                </div>

                <div class="col-lg-6">
                    {!! $value->content_1 !!}
                </div>
                <div class="col-lg-6">
                    {!! $value->content_2 !!}
                </div>
            </div>
        </div>
    </article>

    <article class="container-fluid">
        <div class="container">
            <div class="row text-center pt-5 ">
                @if(!empty($value->subtitle_2))<div class="subtitle">{{$value->subtitle_2}}</div>@endif
                @if(!empty($value->summary_1))<div class="mainTitle fontTwo">{!! $value->summary_1 !!}</div> @endif

                <div class="content mt-5">
                    @if(!empty($value->subtitle_3))<p class="subtitle">{{$value->subtitle_3}}</p>@endif
                    @if(!empty($value->summary_2)) <p>{!! $value->summary_2 !!}</p> @endif
                </div>
            </div>
        </div>
    </article>
@endsection