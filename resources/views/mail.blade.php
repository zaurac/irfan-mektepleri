@extends('layout.mailMaster')
@section('content')
    <table align="center" class="summary content">
        <tbody>
            @foreach($content as $key => $value)
                <tr align="center">
                    <td align="right" width="50%" style="text-align: right"><b>{{$key}} </b></td>
                    <td align="left" width="50%" style="text-align: left">: {!! $value !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection