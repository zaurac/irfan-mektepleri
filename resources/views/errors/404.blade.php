@extends('layout.master')
@section('content')
    <article class="container-fluid bg-primary-color-two pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center mb-4">
                    <h1 class="subtitle">İrfan Mektepleri</h1>
                    <p class="mainTitle fw-bold">404</p>
                </div>
            </div>
        </div>
    </article>

    <article class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-12" style="padding-top: 100px">
                    <h2 class="mainTitle fw-bold">Maalesef Sayfa Bulunamadı</h2>
                </div>
            </div>
        </div>
    </article>
@endsection