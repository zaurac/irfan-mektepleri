@extends('layout.master')
@section('content')
    <article class="container-fluid bg-primary-color-two pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center mb-4">
                    <h1 class="subtitle">İrfan Mektepleri / Sipariş</h1>
                    <p class="mainTitle fw-bold">Satın Almak İçin </p>
                </div>
            </div>
        </div>
    </article>
    
    <article class="container-fluid contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    {!! WebpConvert::createTag('assets/images/book-4.png',['resize' => '1','width' =>[312,546], 'height' => [234,410]],[ "class" => 'image','alt' => 'İrfan Mektepleri İletişim','title' => 'İrfan Mektepleri İletişim'],'','iletisim') !!}
                </div>
                <div class="col-lg-6">
                    <h2 class="mainTitle fontTwo">İLETİŞİM</h2>
                    <div class="alert" style="display: none" id="formAlert" role="alert"></div>
                    <form action="{{route('form.send')}}" method="post" id="formSend">
                        <div class="form-group">
                            <input data-required="true" type="text" name="name_surname" placeholder="Ad & Soyad *" class="form-control">
                        </div>

                        <div class="form-group">
                            <input data-required="true" type="email" name="email" placeholder="E-Mail *" class="form-control">
                        </div>

                        <div class="form-group">
                            <input data-required="true" onkeyup="shorten('phone',10,1);" type="text" name="phone" placeholder="Telefon *" class="form-control">
                        </div>

                        <div class="form-group">
                            <textarea data-required="true" name="content" placeholder="Mesaj *" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>

                        <input type="hidden" name="recaptchaName" value="irfanMektepleriIletisim" autocomplete="off">
                        <input type="hidden" name="recaptcha" autocomplete="off">

                        <button type="submit" class="button-background sendButton">Gönder</button>
                        <img data-src="{{asset('assets/images/loading.gif')}}" class="loadingGif lazy d-none">
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection