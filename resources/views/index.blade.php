@extends('layout.master')
@section('content')
    <article class="container-fluid bg-primary-color-two banner">
        <div id="homePageBannerSlider" class="carousel slide" data-bs-touch="true" data-bs-ride="carousel">
            <div class="carousel-inner container">
                @foreach($sliders as $key => $slider)
                    <div class="carousel-item @if($key == 0) active @endif">
                        <div class="row">
                            <div class="col-lg-6 content @if ($slider->image_left_or_right == 1) order-1 @else order-1 order-lg-0 @endif">
                                @if(!empty($slider->subtitle))<p class="subtitle">{{$slider->subtitle}} </p> @endif
                                @if(!empty($slider->title)) @php $hTagBanner = $key == 0 ? 'h1' : 'h2'; @endphp<{{$hTagBanner}} class="main-title">{{$slider->title}} </{{$hTagBanner}}> @endif
                                @if(!empty($slider->summary))<div class="summary">{!! $slider->summary  !!} </div> @endif
                                @if(!empty($slider->button_text) && !empty($slider->button_slug))
                                    <a href="{{$slider->button_slug}}" class="button-border">{{$slider->button_text}}</a>
                                @endif
                            </div>
                            @if(!empty($slider->image))
                                <div class="col-lg-6 @if ($slider->image_left_or_right == 1) order-0 @else order-0 order-lg-1 @endif">
                                    {!! WebpConvert::createTag('upload/'.$slider->image,['width' =>[635,336], 'height' => [500,265]],['alt' => $slider->title ?? 'İrfan Mektepleri','title' => $slider->title ?? 'İrfan Mektepleri','class' => 'w-100'],'',$slider->title.$key.'-banner') !!}
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            @if(count($sliders) > 1)
                <button class="carousel-control-prev d-none d-xl-inline" type="button" data-bs-target="#homePageBannerSlider" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next d-none d-xl-inline" type="button" data-bs-target="#homePageBannerSlider" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            @endif
        </div>
        <div class="col-12 d-flex pe-5 d-xl-none justify-content-end">
            {!! WebpConvert::createTag('assets/images/icons/touch-right.png',['width' =>[44], 'height' => [65]],['alt' => 'İrfan Mektepleri','title' => 'İrfan Mektepleri'],'','touch-right-icon') !!}
        </div>
    </article>

    <article class="container-fluid short-content">
        <div class="container">
            @foreach($informations as $key => $information)
                <div class="row item mb-5">
                    <div class="col-lg-6 image @if ($key % 2 == 0) order-0 order-lg-0 @else order-0 order-lg-1 @endif">
                        {!! WebpConvert::createTag('upload/'.$information->image,['width' =>[636,336], 'height' => [550,291]],['alt' => $information->title ?? 'İrfan Mektepleri','title' => $information->title ?? 'İrfan Mektepleri'],'',$information->slug.'-image') !!}
                    </div>
                    <div class="col-lg-6 content @if ($key % 2 == 0) order-1  @else order-1 order-lg-0 @endif">
                        <h2 class="main-title offset-lg-2">{{$information->title}}</h2>
                        <div class="row">
                            <div class="col-lg-2 d-none d-lg-inline">
                                {!! WebpConvert::createTag('upload/'.$information->icon,['width' =>[65], 'height' => [65]],['alt' => $information->title ?? 'İrfan Mektepleri','title' => $information->title ?? 'İrfan Mektepleri'],'',$information->slug.'-icon') !!}
                            </div>
                            <div class="col-12 col-lg-10">
                                <p class="mb-3">{!! $information->summary  !!}</p>
                                <a href="{{route('content.detail',$information->slug)}}" class="button-border">Detay</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </article>

    <article class="container-fluid pearls">
        <div class="container">
            <div class="row justify-content-center">
                <h2 class="main-title">GÖNÜLDEN DÖKÜLEN İNCİLER</h2>
                <div class="line"></div>
                <p class="sub-title">Ey Salik Bilensin ki;</p>
                @foreach($pearls as $pearl)
                    <div class="col-xl-4 col-lg-6">
                        <div class="pearl">
                            {{$pearl->summary}} <br>...
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </article>
@endsection