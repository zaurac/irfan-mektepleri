$('#formSend').submit(function (e)
{
    e.preventDefault();
    let formSelector = '#formSend';
    let saveButton   = $(formSelector+' .sendButton');
    let loading      = $(formSelector+' .loadingGif');
    let formData     = new FormData(this);
    let formRequired = $(formSelector + ' [data-required="true"]');

    saveButton.addClass('d-none');
    loading.removeClass('d-none');
    $(formSelector+' .is-invalid').removeClass('is-invalid');
    $(formSelector+' .is-valid').removeClass('is-valid');

    if (formRequired.length > 1)
    {
        let result;

        for (var i = 0; i < formRequired.length; i++)
        {
            let placeholder  = formRequired.eq(i).attr('placeholder');
            let inputName    = formRequired.eq(i).attr('name');
            let errorMessage = placeholder + ' alanı zorunludur.';


            if (formRequired.eq(i).val() == '')
            {
                errorFormElement(formSelector,'[name="'+inputName+'"]',errorMessage);
                result = 1;
            }
            else
            {
                successFormElement(formSelector,'[name="'+inputName+'"]',errorMessage);
            }

            if (inputName == 'phone')
            {
                if (formRequired.eq(i).val().substring(0,1) != 5)
                {
                    let phoneErrorMessage = 'Lütfen doğru formatta giriniz. Örn : (5xxxxxxxxx)';
                    errorFormElement(formSelector,'[name="'+inputName+'"]',phoneErrorMessage);
                    result = 1;
                }
            }
        }

        if (result == 1)
        {
            saveButton.removeClass('d-none');
            loading.addClass('d-none');
            return false;
        }
    }

    grecaptcha.ready(function()
    {
        let action = $(formSelector+' input[name="recaptchaName"]').val();

        grecaptcha.execute('6LerUUUoAAAAAB41bMoWdmi75MCm0Ar3xrJ45_Sw', {action: action})
            .then(function(token)
            {
                $(formSelector+' input[name="recaptcha"]').val(token);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: $(formSelector).attr('method'),
                    url:  $(formSelector).attr('action'),
                    data: formData,
                    dataType: "json",
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function (response)
                    {
                        saveButton.removeClass('d-none');
                        loading.addClass('d-none');
                        messageAlert(1,response.message);

                        if (response.route != undefined)
                        {
                            location = response.route;
                            setTimeout(function() {location}, 2000);
                        }
                        else
                        {
                            setTimeout(function() {location.reload()}, 5000);
                        }
                    },
                    error : function (response)
                    {
                        saveButton.removeClass('d-none');
                        loading.addClass('d-none');
                        $(formSelector+" .invalid-feedback").remove();

                        if (response.responseJSON.result == 2)
                        {
                            $(formSelector).addClass('was-validated');

                            $.each(response.responseJSON.message, function(i, item)
                            {
                                $(formSelector+' [name="'+i+'"]').addClass('is-invalid');
                                $(formSelector+' [name="'+i+'"]').closest('div.form-group').append('<div class="invalid-feedback">'+item[0]+'</div>');
                            });
                        }
                        else if (response.responseJSON.message)
                        {
                            messageAlert(2,response.responseJSON.message);
                        }
                        else
                        {
                            messageAlert(2,response.message);
                        }
                    }
                });
            });
    });
});

function shorten(inputName,length,type)
{
    var selected = $('input[name="'+inputName+'"]').val();

    if (selected.length > length)
    {
        var newValue = selected.substring(0,length);

        if (type == 1 && newValue[0] == 0)
        {
            var newValue = selected.substring(1,(length+1));
        }
        $('input[name="'+inputName+'"]').val(newValue);
    }
}
function errorFormElement(formSelector,selector,message)
{
    $(formSelector+' '+selector).next('div.invalid-feedback').remove();
    $(formSelector+' '+selector).removeClass('is-invalid');
    $(formSelector+' '+selector).removeClass('is-valid');
    $(formSelector+' '+selector).addClass('is-invalid');
    $(formSelector+' '+selector).closest('div.form-group').append('<div class="invalid-feedback">'+message+'</div>');
    $('html,body').animate({scrollTop: ($(formSelector).offset().top - 110)}, 200);
}

function successFormElement(formSelector,selector,message)
{
    $(formSelector+' '+selector).next('div.invalid-feedback').remove();
    $(formSelector+' '+selector).removeClass('is-invalid');
    $(formSelector+' '+selector).removeClass('is-valid');
    $(formSelector+' '+selector).addClass('is-valid');
    $(formSelector+' '+selector).closest('div.form-group').append('<div class="invalid-feedback">'+message+'</div>');
    $('html,body').animate({scrollTop: ($(formSelector).offset().top - 110)}, 200);
}

function messageAlert(status,message,title = null,formId = null)
{
    let statusName      = ['','success','danger','warning'];
    let alertHtml       = '';
    let formAlert       = document.getElementById('formAlert');
    formAlert.className = '';

    if (title)
    {
        alertHtml += '<p class="alert-heading">'+title+'</p>';
    }

    alertHtml           += '<p>'+message+'</p>';
    formAlert.className  = 'alert alert-'+statusName[status];
    formAlert.innerHTML  = alertHtml;

    $('#formAlert').fadeIn('slow');
    $('html,body').animate({scrollTop: ($('#formAlert').offset().top - 110)}, 200);
    setTimeout(function() {$('#formAlert').fadeOut('slow');}, 15000);
}

(function () {
    var callback_loaded = function (element) {
        $('[data-ll-status="loaded"]').removeClass("lazy");
    };
    new LazyLoad({ elements_selector: ".lazy",callback_loaded: callback_loaded });
})();

$('form').click(function ()
{
    var url       = 'https://www.google.com/recaptcha/api.js?render=6LerUUUoAAAAAB41bMoWdmi75MCm0Ar3xrJ45_Sw';
    var className = 'recaptchaCreate';
    loadNewScriptCreate(className,url);
});

function loadNewScriptCreate(className,url)
{
    if ($('.'+className).length == 0)
    {
        var recaptchaCreate = document.createElement("script");
        recaptchaCreate.src = url;
        recaptchaCreate.className = className;
        document.body.appendChild(recaptchaCreate);
    }
}