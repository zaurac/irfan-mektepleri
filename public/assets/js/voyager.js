var additionalConfig = {
    entity_encoding: 'raw'
}

$.extend(additionalConfig, {})

tinymce.init(window.voyagerTinyMCE.getConfig(additionalConfig));