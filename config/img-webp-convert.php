<?php

return
    [
        "disk"          => 'cache',
        "no-image"      => null,
        "loading-image" => '/assets/images/loading.gif'
    ];