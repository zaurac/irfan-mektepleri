<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;


class Slider extends Model
{
    public static function boot()
    {
        parent::boot();


        static::updated(function ($query)
        {
            $file = new Filesystem;
            $file->cleanDirectory('upload/cache');
        });
    }
}
