<?php

namespace App\Http\Controllers;

use App\Biography;
use App\Content;
use App\Form;
use App\Information;
use App\Mail\SendMail;
use App\Models\Setting;
use App\Pearl;
use App\Slider;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class MainController extends Controller
{
    public function index()
    {
        $sliders      = Slider::where('status',1)->orderBy('order','asc')->get();
        $informations = Information::where('status',1)->orderBy('order','asc')->get();
        $pearls       = Pearl::where('status',1)->orderBy('order','asc')->get();


        $this->seo();

        return view('index',compact('sliders','informations','pearls'));
    }

    public function biography()
    {
        $value = Biography::find(1);

        $this->seo('Biyografi - İrfan Mektepleri');

        return view('biography',compact('value'));
    }

    public function contents()
    {
        $values = Content::where('status',1)->orderBy('order','asc')->get();
        $this->seo('İçindekiler - İrfan Mektepleri');

        return view('contents',compact('values'));
    }

    public function detail($slug)
    {
        $value = Information::where('slug',$slug)->first();

        if (empty($value))
        {
            $this->seo('Sayfa Bulunamadı - İrfan Mektepleri',null);
            abort(404);
        }

        $this->seo($value->seo_title.' - İrfan Mektepleri',$value->seo_description);

        return view('detail',compact('value'));
    }

    public function contact()
    {

        $this->seo('İletişim - İrfan Mektepleri');

        return view('contact');
    }

    public function seo($title = null,$description = null)
    {
        $title        = empty($title) ? 'İrfan Mektepleri' : $title;
        $description  = empty($description) ? 'Allah ve Rasûlü’nün sevgisiyle kaleme aldığı, dinimizin, manevi boyutunun, Anadolu irfanıyla canlanması ve yaşanması için yıllarını ve zamanını vakfederek hazırladığı, ilhamlarla gönlünden kalemine dökülen bir kitaptır.' : $description;
        //OpenGraph::addImage(adminImage($page->image,1));
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
    }

    public function googleCaptchaControl($recaptcha)
    {
        $googleData = array(
            'secret' 	=> '6LerUUUoAAAAAB41bMoWdmi75MCm0Ar3xrJ45_Sw',
            'response'	=> $recaptcha,
            'remoteip'	=> $_SERVER['REMOTE_ADDR']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($googleData));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $googleReq = curl_exec($curl);
        $googleRes = json_decode($googleReq);

        if(!$googleRes->success)
        {
            return response()->json(['result' => 0,'message' => 'Güvenlik kontrolü sebebiyle sayfa yenilenecek. Lütfen bekleyiniz.'],403);
        }
    }

    public function send(Request $request)
    {
        $this->googleCaptchaControl($request->get('recaptcha'));

        try
        {
            $rules  =
                [
                    "name_surname"  => 'required',
                    "email"         => 'required|email',
                    "phone"         => 'required|digits:10',
                    "content"       => 'required',
                ];

            $attribute  =
                [
                    "name_surname"  => 'Ad & Soyad',
                    "email"         => 'E-Mail',
                    "phone"         => 'Telefon',
                    "content"       => 'Mesaj',
                ];

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($attribute);

            if ($validator->fails())
            {
                return response()->json(
                    [
                        'result' => 2,
                        'message' => $validator->errors()
                    ],403
                );
            }

            $form               = new Form();
            $form->name_surname = $request->get('name_surname');
            $form->email        = $request->get('email');
            $form->phone        = $request->get('phone');
            $form->content      = $request->get('content');
            $form->save();

            $data   =
                [
                    'subject' => 'Yeni form kaydı geldi.',
                    'content' =>
                        [
                            'Ad & Soyad'       => $request->get('name_surname'),
                            'Telefon Numarası' => $request->get('phone') ?? '-',
                            'E-Mail'           => $request->get('email'),
                            'Mesaj'            => $request->get('content'),
                        ]
                ];

            $this->sendMail($data);

            return response()->json(
                [
                    'result'  => 1,
                    'message' => 'Form gönderimi başarılı olmuştur. En kısa süre içerisinde sizinle irtibata geçilecektir.'
                ]
            );
        }
        catch (\Exception $e)
        {
            return response()->json(
                [
                    'result'  => 0,
                    'message' => 'İşlem başarısız olmuştur. Lütfen daha sonra tekrar deneyiniz.'
                ]
            );
        }
    }

    public function sendMail($data)
    {
        $mailList  = setting('site.mail_list');

        Mail::to($mailList)->send(new SendMail($data));
    }
}
