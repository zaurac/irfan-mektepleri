<?php

namespace App\Http\Middleware;

use App\Information;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class Variables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $footerContents = Information::where('status',1)->where('footer',1)->orderBy('order','asc')->get();

        View::share([
            'footerContents' => $footerContents,
        ]);

        return $next($request);
    }
}
