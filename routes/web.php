<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\MainController::class,'index'])->name('index');
Route::get('/biyografi', [\App\Http\Controllers\MainController::class,'biography'])->name('biography');
Route::get('/icindekiler', [\App\Http\Controllers\MainController::class,'contents'])->name('contents');
Route::get('/iletisim', [\App\Http\Controllers\MainController::class,'contact'])->name('contact');
Route::get('/upload-cache-clear', [\App\Http\Controllers\MainController::class,'uploadCacheClear'])->name('uploadCacheClear');
Route::post('/send', [\App\Http\Controllers\MainController::class,'send'])->name('form.send');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/{slug}', [\App\Http\Controllers\MainController::class,'detail'])->name('content.detail');